package br.com.spring.estudo.estudos.findall.service;


import br.com.spring.estudo.estudos.model.AluguelModel;
import br.com.spring.estudo.estudos.repositores.AluguelRepository;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class EnderecoFindAllServiceTest {
    @Mock
    private AluguelRepository aluguelRepository;


    @Test
    @DisplayName("Buscando todos alugueis")
    public void whenFindall_shouldReturnUmAluguel() {
        List<AluguelModel> items = aluguelRepository.findAll();
        assertEquals(3,items.size());
    }

}
