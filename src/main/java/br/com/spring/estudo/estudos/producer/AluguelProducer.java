package br.com.spring.estudo.estudos.producer;

import br.com.spring.estudo.estudos.dtos.AluguelDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aluguelsqs")
public class AluguelProducer {
    private static final Logger logger = LoggerFactory.getLogger(AluguelProducer.class);

    @Autowired
    private QueueMessagingTemplate queueMessagingTemplate;

    @Value("${cloud.aws.end-point.uri}")
    private String endPoint;


    @PostMapping("/message")
    public AluguelDto sendMessage(@RequestBody AluguelDto aluguel) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writeValueAsString(aluguel);
            logger.info("Message passei aqui ");
            queueMessagingTemplate.send(endPoint, MessageBuilder.withPayload(jsonString).build());
            logger.info("Message sent successfully  " + jsonString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aluguel;
    }
}
