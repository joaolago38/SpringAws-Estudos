package br.com.spring.estudo.estudos.model;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "rental")
@Getter
@Setter
@Builder
public class AluguelModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rental_rental_id_seq")
    @SequenceGenerator(name = "rental_rental_id_seq", sequenceName = "rental_rental_id_seq", allocationSize = 1)
    private Integer rentalId;
    @Column(name = "rentalDate")
      private String rentalDate;
    @Column(name = "returnDate")
    private String returnDate;
    @Column(name = "customerId")
    private Integer customerId;
    @Column(name = "staffId")
    private Integer staffId;
    @Column(name = "inventoryId")
    private Integer inventoryId;
    @Column(name = "lastpdate")
    private String lastUpdate;


    public AluguelModel(Integer rentalId,
                        String rentalDate,
                        String returnDate,
                        Integer customerId,
                        Integer staffId,
                        Integer inventoryId,
                        String lastUpdate) {
        this.rentalId = rentalId;
        this.rentalDate = rentalDate;
        this.returnDate = returnDate;
        this.customerId = customerId;
        this.staffId = staffId;
        this.inventoryId = inventoryId;
        this.lastUpdate = lastUpdate;
    }

    public AluguelModel() {

    }


}
