package br.com.spring.estudo.estudos.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteDto {

    private Integer customerId;
    private Long storeId;
    private String firstName;
    private String lastName;
    private String email;
    private Long addressId;
    private Boolean activebool;
    private String createDate;
    private Boolean active;


}
