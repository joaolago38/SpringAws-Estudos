package br.com.spring.estudo.estudos.repositores;

import br.com.spring.estudo.estudos.enumeracao.ERole;
import br.com.spring.estudo.estudos.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
