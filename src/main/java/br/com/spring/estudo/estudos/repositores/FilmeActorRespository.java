package br.com.spring.estudo.estudos.repositores;



import br.com.spring.estudo.estudos.model.FilmeAtorModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmeActorRespository extends JpaRepository<FilmeAtorModel, Integer  > {

}
