package br.com.spring.estudo.estudos.repositores;

import br.com.spring.estudo.estudos.model.PessoaModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PessoaRepository extends JpaRepository<PessoaModel, Integer  > {

}
