package br.com.spring.estudo.estudos.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AluguelDto {

    private Integer rentalId;
    private String rentalDate;
    private String returnDate;
    private Integer customerId;
    private Integer staffId;
    private Integer inventoryId;
    private String lastUpdate;


}
