package br.com.spring.estudo.estudos.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Table(name = "inventory")
@Getter
@Setter
@Builder
public class InventarioModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE , generator="inventory_inventory_id_seq")
    @SequenceGenerator(name="inventory_inventory_id_seq", sequenceName="inventory_inventory_id_seq",allocationSize = 1)
    private Integer inventoryId;
    @Column(name = "film_id")
    private Integer filmId;
    @Column(name = "store_id")
    private Integer storeId;
    @Column(name = "last_update")
    private String lastUpdate;

    public InventarioModel() {

    }

    public InventarioModel(Integer inventoryId, Integer filmId, Integer storeId, String lastUpdate) {
        this.inventoryId = inventoryId;
        this.filmId = filmId;
        this.storeId = storeId;
        this.lastUpdate = lastUpdate;
    }
}
