package br.com.spring.estudo.estudos.enumeracao;

public enum ERole {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
