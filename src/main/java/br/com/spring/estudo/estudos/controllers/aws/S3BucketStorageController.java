package br.com.spring.estudo.estudos.controllers.aws;


import br.com.spring.estudo.estudos.enumeracao.FileMediaType;
import br.com.spring.estudo.estudos.services.S3BucketStorageService;
import lombok.SneakyThrows;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/s3bucketstorage")
public class S3BucketStorageController {

    @Autowired
    private S3BucketStorageService service;


    @GetMapping("/{bucketName}")
    public ResponseEntity<?> listFiles(
        @PathVariable("bucketName") String bucketName
    ) {
        val body = service.listFiles(bucketName);
        return ResponseEntity.ok(body);
    }
    
    @PostMapping("/{bucketName}/upload")
    @SneakyThrows(IOException.class)
    public ResponseEntity<?> uploadFile(
        @PathVariable("bucketName") String bucketName,
        @RequestPart("file") MultipartFile file,
        @RequestPart("fileName") String fileName
    ) {
        service.uploadFile(bucketName, fileName, file.getSize(), file.getContentType(), file.getInputStream());
        return ResponseEntity.ok().build();
    }

    @SneakyThrows
    @GetMapping("/{bucketName}/download/{fileName}")
    public ResponseEntity<?> downloadFile(
        @PathVariable("bucketName") String bucketName,
        @PathVariable("fileName") String fileName
    ) {
        val body = service.downloadFile(bucketName, fileName);

        return ResponseEntity.ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
            .contentType(FileMediaType.fromFilename(fileName))
            .body(body.toByteArray());
    }

    @DeleteMapping("/{bucketName}/{fileName}")
    public ResponseEntity<?> deleteFile(
        @PathVariable("bucketName") String bucketName,
        @PathVariable("fileName") String fileName
    ) {
        service.deleteFile(bucketName, fileName);
        return ResponseEntity.ok().build();
    }
}
