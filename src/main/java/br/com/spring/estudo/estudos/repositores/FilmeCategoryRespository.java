package br.com.spring.estudo.estudos.repositores;



import br.com.spring.estudo.estudos.model.FilmeCategoriaModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmeCategoryRespository extends JpaRepository<FilmeCategoriaModel, Integer  > {


}
