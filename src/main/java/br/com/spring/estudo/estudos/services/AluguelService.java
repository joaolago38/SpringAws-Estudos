package br.com.spring.estudo.estudos.services;


import br.com.spring.estudo.estudos.model.AluguelModel;
import br.com.spring.estudo.estudos.repositores.AluguelRepository;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AluguelService {
    final AluguelRepository aluguelRepository;

    static List<AluguelModel> aluguelModels = new ArrayList<AluguelModel>();

    public AluguelService(AluguelRepository aluguelRepository) {
        this.aluguelRepository = aluguelRepository;
    }


    @Transactional
    public AluguelModel save(AluguelModel aluguelModel) {
        return aluguelRepository.save(aluguelModel);
    }

    public List<AluguelModel> findAll() {
        return aluguelRepository.findAll();
    }

    public Optional<AluguelModel> findById(Integer filmId) {
        return aluguelRepository.findById(filmId);
    }
    @Transactional
    public void delete(AluguelModel aluguelModel) {
        aluguelRepository.delete(aluguelModel);
    }

    public List<AluguelModel> findByTitleContaining(Integer rentalId) {
        return (List<AluguelModel>) aluguelModels.stream().filter(tutorial -> aluguelModels.contains(rentalId));
    }


}
