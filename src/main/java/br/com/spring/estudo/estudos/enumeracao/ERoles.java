package br.com.spring.estudo.estudos.enumeracao;

public enum ERoles {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
